﻿using DolphiOrdersCore.Infrastructure.ViewModels;
using DolphiOrdersCore.Miscellaneous;
using DolphiOrdersCore.Models.Concrete;
using DolphiOrdersCore.Models.Infrastructure;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DolphiOrdersCore.Components
{
    public class CartSummary: ViewComponent
    {
        private readonly ProductContext db;
        private HttpContext httpContext;

        public CartSummary(ProductContext context, IHttpContextAccessor httpContextAccessor)
        {
            db = context;
            httpContext = httpContextAccessor.HttpContext;
        }

        public IViewComponentResult Invoke()
        {
            if (httpContext == null)
            {
                throw new ArgumentNullException(nameof(httpContext));
            }
            Cart cart = httpContext.Session.GetObjectFromJson<Cart>("Cart");
            if (cart == null)
            {
                cart = new Cart();
                httpContext.Session.SetObjectAsJson("Cart", cart);
            }
            return View("CartSummaryView", new CartViewModel() { Cart = cart, ReturnUrl = httpContext.Request.Path.Value });
        }
    }
}
