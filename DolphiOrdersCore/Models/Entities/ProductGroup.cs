﻿using DolphiOrdersCore.Models.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DolphiOrdersCore.Models.Entities
{
    public class ProductGroup: IExchangable<ProductGroup>
    {
        public Guid Id { get; set; }
        public string Code1C { get; set; }
        public string Name { get; set; }
        public bool DeletionMark { get; set; }

        public void FillForExchanger(ProductGroup sourse)
        {
            Code1C = sourse.Code1C;
            Name = sourse.Name;
            DeletionMark = sourse.DeletionMark;
        }
    }
}
