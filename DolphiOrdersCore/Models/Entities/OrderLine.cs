﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DolphiOrdersCore.Models.Entities
{
    public class OrderLine
    {
        public Guid Id { get; set; }

        public Guid OrderId { get; set; }

        public Guid ProductId { get; set; }
        public Product Product { get; set; }

        public Guid MeasureId { get; set; }
        public Measure Measure { get; set; }

        public decimal Quantity { get; set; }
        public decimal Price { get; set; }
        public decimal Sum { get; set; }
    }
}
