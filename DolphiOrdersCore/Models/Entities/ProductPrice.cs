﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace DolphiOrdersCore.Models.Entities
{
    public class ProductPrice
    {
        [Key]
        public Guid ProductId { get; set; }
        public Product Product { get; set; }

        [Key]
        public Guid PriceTypeId { get; set; }
        public PriceType PriceType { get; set; }

        public decimal Price { get; set; }

        public Guid MeasureId { get; set; }
        public Measure Measure { get; set; }
    }
}
