﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DolphiOrdersCore.Models.Entities
{
    public class User: IdentityUser
    {
        Guid CustomerId { get; set; }
        public Customer Customer { get; set; }

    }
}
