﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DolphiOrdersCore.Models.Entities
{
    public class Outlet
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Code1C { get; set; }
        public bool DeletionMark { get; set; }

        public Guid CustomerId { get; set; }
        public Customer Customer { get; set; }
    }
}
