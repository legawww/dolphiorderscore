﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DolphiOrdersCore.Models.Entities
{
    public class Order
    {
        public Guid Id { get; set; }
        public string OrderNumber1C { get; set; }
        public bool DeletionMark { get; set; }

        public Guid OutletId { get; set; }
        public Outlet Outlet { get; set; }

        public List<OrderLine> Lines { get; set; }
    }
}
