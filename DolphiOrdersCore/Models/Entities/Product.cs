﻿using DolphiOrdersCore.Models.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DolphiOrdersCore.Models.Entities
{
    public class Product : IExchangable<Product>
    {
        public Guid Id { get; set; }
        public string Code1C { get; set; }
        public string Article { get; set; }
        public string Name { get; set; }
        public bool DeletionMark { get; set; }
        public bool IsFolder { get; set; }
        public bool ProductDoesNotUse { get; set; }
        
        public Guid? ParentId { get; set; }
        public Product Parent { get; set; }

        public Guid? MeasureId { get; set; }
        public Measure Measure { get; set; }

        public Guid? ProductGroupId { get; set; }
        public ProductGroup ProductGroup { get; set; }

        public List<ProductPrice> ProductPrices { get; set; }

        public void FillForExchanger(Product sourse)
        {
            Code1C = sourse.Code1C;
            Name = sourse.Name;
            DeletionMark = sourse.DeletionMark;
            IsFolder = sourse.IsFolder;
            ParentId = sourse.ParentId;

            Article = sourse.Article;
            ProductGroupId = sourse.ProductGroupId;
            MeasureId = sourse.MeasureId;
            ProductDoesNotUse = sourse.ProductDoesNotUse;

        }
    }
}
