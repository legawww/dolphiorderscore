﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DolphiOrdersCore.Models.Entities
{
    public class PriceType
    {
        public Guid Id { get; set; }
        public string Code1C { get; set; }
        public string Name { get; set; }
        public bool DeletionMark { get; set; }
    }
}
