﻿using DolphiOrdersCore.Models.Abstract;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace DolphiOrdersCore.Models.Entities
{
    public class Measure : IExchangable<Measure>
    {
        [Key]
        public Guid Id { get; set; }
        public string Code1C { get; set; }
        [Display(Name = "Ед. изм.")]
        public string Name { get; set; }
        public decimal Rate { get; set; }
        public bool DeletionMark { get; set; }

        public Guid ProductId { get; set; }
        public Product Product { get; set; }

        public void FillForExchanger(Measure sourse)
        {
            Code1C = sourse.Code1C;
            Name = sourse.Name;
            DeletionMark = sourse.DeletionMark;
            ProductId = sourse.ProductId;
            Rate = sourse.Rate;
        }
    }
}
