﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DolphiOrdersCore.Models.Entities
{
    public class Customer
    {
        public Guid Id { get; set; }
        public string Code1C { get; set; }
        public string Name { get; set; }
        public string EDRPOU { get; set; }
        public bool DeletionMark { get; set; }

        public Guid? PriceTypeId { get; set; }
        public PriceType PriceType { get; set; }

        public Guid? TradeGroupId { get; set; }
        public Customer TradeGroup { get; set; }

    }
}
