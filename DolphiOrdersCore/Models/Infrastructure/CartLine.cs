﻿using DolphiOrdersCore.Models.Entities;

namespace DolphiOrdersCore.Models.Infrastructure
{
    public class CartLine
    {
        public Product Product { get; set; }
        public int Quantity { get; set; }
        public decimal Price { get; set; }

        public decimal GetLineSum()
        {
            return Price * Quantity;
        }
    }
}