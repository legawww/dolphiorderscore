﻿using DolphiOrdersCore.Models.Entities;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DolphiOrdersCore.Models.Infrastructure
{
    public class Cart
    {

        private List<CartLine> _cartLines;
        public IEnumerable<CartLine> Lines { get { return _cartLines; } }

        public Cart()
        {
            _cartLines = new List<CartLine>();
        }

        public void AddItem(Product product, int quantity, decimal price)
        {
            CartLine cartLine = _cartLines.FirstOrDefault(cl => cl.Product.Id == product.Id && cl.Price == price);
            if (cartLine == null)
            {
                _cartLines.Add(new CartLine { Product = product, Price = price, Quantity = quantity });
            }
            else
            {
                cartLine.Quantity += quantity;
            }
        }

        public void RemoveItem(Product product)
        {
            _cartLines.RemoveAll(cl => cl.Product.Id == product.Id);
        }

        public void Clear()
        {
            _cartLines.Clear();
        }

        public decimal GetTotalSum()
        {
            return _cartLines.Sum(cl => cl.GetLineSum());
        }

        public int GetTotalQuantity()
        {
            return _cartLines.Sum(cl => cl.Quantity);
        }

        public CartLine SetProductQuantity(Guid id, int quantity)
        {
            CartLine line = _cartLines.FirstOrDefault(cl => cl.Product.Id == id);
            if (line == null)
            {
                throw new ArgumentException("There are no products in card with id " + id);
            }
            line.Quantity = quantity;
            return line;
        }
    }
}
