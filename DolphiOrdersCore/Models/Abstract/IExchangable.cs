﻿using DolphiOrdersCore.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DolphiOrdersCore.Models.Abstract
{
    interface IExchangable<T> where T: class
    {
        void FillForExchanger(T sourse);
    }
}
