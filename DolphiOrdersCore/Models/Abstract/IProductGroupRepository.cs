﻿using DolphiOrdersCore.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DolphiOrdersCore.Models.Abstract
{
    public interface IProductGroupRepository
    {
        IEnumerable<ProductGroup> ProductGroups { get; }
        ProductGroup GetProductGroup(Guid id);
        ProductGroup GetProductGroup(string code1C);
        Guid Create(ProductGroup productGroup);
        bool IsExist(string code1C);
        bool IsExist(Guid Id);
    }
}
