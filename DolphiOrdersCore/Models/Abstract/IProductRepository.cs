﻿using DolphiOrdersCore.Models.Entities;
using System;
using System.Collections.Generic;

namespace DolphiOrdersCore.Models.Abstract
{
    public interface IProductRepository
    {
        IEnumerable<Product> Products { get; }
        Product GetProduct(Guid id);
        Product GetProduct(string code1C);
        Guid Create(Product product);
        bool IsExist(string code1C);
        bool IsExist(Guid Id);
    }
}
