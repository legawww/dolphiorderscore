﻿using DolphiOrdersCore.Models.Entities;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DolphiOrdersCore.Models.Concrete
{
    public class ProductContext: IdentityDbContext<User>
    {
        public DbSet<Product> Products { get; set; }
        public DbSet<Measure> Measures { get; set; }
        public DbSet<Customer> Customers { get; set; }
        public DbSet<Outlet> Outlets { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<OrderLine> OrderLines { get; set; }
        public DbSet<PriceType> PriceTypes { get; set; }
        public DbSet<ProductPrice> ProductPrices { get; set; }
        public DbSet<ProductGroup> ProductGroups { get; set; }


        public ProductContext(DbContextOptions<ProductContext> options): base(options)
        {
            
        }
        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            builder.Entity<Product>().Property(p => p.Id).ValueGeneratedNever();
            builder.Entity<ProductGroup>().Property(p => p.Id).ValueGeneratedNever();
            builder.Entity<Customer>().Property(p => p.Id).ValueGeneratedNever();
            builder.Entity<Measure>().Property(p => p.Id).ValueGeneratedNever();
            builder.Entity<Outlet>().Property(p => p.Id).ValueGeneratedNever();
            builder.Entity<PriceType>().Property(p => p.Id).ValueGeneratedNever();

            builder.Entity<Measure>().HasOne(m => m.Product).WithMany().HasForeignKey("ProductId").OnDelete(DeleteBehavior.Cascade);
            builder.Entity<Product>().HasOne(p=>p.Measure).WithOne().HasForeignKey<Product>(f=>f.MeasureId);
            builder.Entity<OrderLine>().HasOne(ol => ol.Product).WithMany().HasForeignKey("ProductId").OnDelete(DeleteBehavior.ClientSetNull);
            builder.Entity<ProductPrice>().HasKey(pp => new { pp.ProductId, pp.PriceTypeId });
            builder.Entity<ProductPrice>().HasOne(pp => pp.Product).WithMany(p=>p.ProductPrices).HasForeignKey("ProductId").OnDelete(DeleteBehavior.ClientSetNull);
        }
    }
}
