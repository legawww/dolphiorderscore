﻿using DolphiOrdersCore.Models.Abstract;
using DolphiOrdersCore.Models.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DolphiOrdersCore.Models.Concrete
{
    public class EFProductGroupRepository : IProductGroupRepository
    {
        private readonly ProductContext db;
        public EFProductGroupRepository(ProductContext context)
        {
            db = context;
        }
        public IEnumerable<ProductGroup> ProductGroups {
            get {
                return db.ProductGroups;
            } }

        public Guid Create(ProductGroup productGroup)
        {
            ProductGroup finded = GetProductGroup(productGroup.Id);
            
            if (finded==null)
            {
                db.ProductGroups.Add(productGroup);
                db.SaveChanges();
            }
            return productGroup.Id;
        }

        public ProductGroup GetProductGroup(Guid id)
        {
            return db.ProductGroups.Find(id);
        }

        public ProductGroup GetProductGroup(string code1C)
        {
            return db.ProductGroups.FirstOrDefault(pg => pg.Code1C == code1C);
        }
        public bool IsExist(string code1C)
        {
            return db.ProductGroups.Any(pg => pg.Code1C == code1C);
        }
        public bool IsExist(Guid id)
        {
            return db.ProductGroups.Any(pg => pg.Id == id);
        }
    }
}
