﻿using DolphiOrdersCore.Models.Abstract;
using DolphiOrdersCore.Models.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DolphiOrdersCore.Models.Concrete
{
    public class EFProductRepository : IProductRepository
    {
        private readonly ProductContext db;
        private readonly IProductGroupRepository _productGroupRepository;
        public EFProductRepository(ProductContext context, IProductGroupRepository productGroupRepository)
        {
            db = context;
            _productGroupRepository = productGroupRepository;
        }
        public IEnumerable<Product> Products {
            get {
                return db.Products;
            } }

        public Guid Create(Product product)
        {
            Product finded = GetProduct(product.Id);
            //_productGroupRepository.Create(product.ProductGroup);
            
            if (finded==null)
            {
                db.ProductGroups.Attach(product.ProductGroup);
                db.Products.Add(product);
                db.SaveChanges();
            }
            
            
            return product.Id;
        }

        public Product GetProduct(Guid id)
        {
            return db.Products.Find(id);
        }

        public Product GetProduct(string code1C)
        {
            return db.Products.FirstOrDefault(pg => pg.Code1C == code1C);
        }
        public bool IsExist(string code1C)
        {
            return db.Products.Any(pg => pg.Code1C == code1C);
        }
        public bool IsExist(Guid id)
        {
            return db.Products.Any(pg => pg.Id == id);
        }
    }
}
