﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DolphiOrdersCore.Models.Infrastructure;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace DolphiOrdersCore.Infrastructure.ModelBinders
{
    public class CartmModelBinderProvider : IModelBinderProvider
    {
        public IModelBinder GetBinder(ModelBinderProviderContext context)
        {
            return context.Metadata.ModelType == typeof(Cart) ? new CartModelBinder() : null;
        }
    }
}
