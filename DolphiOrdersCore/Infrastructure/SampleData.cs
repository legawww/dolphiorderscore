﻿using DolphiOrdersCore.Models.Concrete;
using DolphiOrdersCore.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DolphiOrdersCore.Infrastructure
{
    public static class SampleData
    {
        public static void Initialize(ProductContext context)
        {
            if (!context.Products.Any())
            {
                #region Типы цен
                PriceType PriceType1 = new PriceType
                {
                    Code1C = "DP01658",
                    Name = "Алтіма ТОВ (0000504)"
                };
                PriceType PriceType2 = new PriceType
                {
                    Code1C = "DP01342",
                    Name = "Алмаз Ко ПП (DPR3985)"
                };
                #endregion
                #region Контрагенты
                Customer customer1 = new Customer
                {
                    Name = "Алтіма ТОВ",
                    EDRPOU = "21911728",
                    Code1C = "0000504",
                    PriceType = PriceType1
                };
                Customer customer2 = new Customer
                {
                    Name = "Алмаз Ко ПП",
                    EDRPOU = "31911422",
                    Code1C = "0000488",
                    PriceType = PriceType2
                };
                #endregion


                #region Номенклатурные группы
                ProductGroup pg1 = new ProductGroup
                {
                    Code1C = "00058",
                    Name = "Презервативи DOLPHI"
                };
                ProductGroup pg2 = new ProductGroup
                {
                    Code1C = "00092",
                    Name = "Прибори Bionime"
                };
                #endregion

                #region Номенклатура
                Product p1 = new Product
                {
                    Name = @"Презервативи DOLPHI Анатомічні надтонкі 12 шт медпак",
                    Article = @"DOLPHI/Анатом/надтон/12",
                    Code1C = "ПDК00033",
                    ProductGroup = pg1
                };
                Product p2 = new Product
                {
                    Name = @"Презервативи DOLPHI Анатомічні надтонкі 3 шт медпак",
                    Article = @"DOLPHI/Анатом/надтон/3",
                    Code1C = "ПDК00025",
                    ProductGroup = pg1
                };
                Product p3 = new Product
                {
                    Name = @"Презервативи DOLPHI Ребристі 3 шт",
                    Article = @"DOLPHI/Ребристіі/3",
                    Code1C = "COM01592",
                    ProductGroup = pg1
                };
                Product p4 = new Product
                {
                    Name = @"Глюкометр Rightest GM 110",
                    Article = @"GM110",
                    Code1C = "ГЛ000019",
                    ProductGroup = pg2
                };
                Product p5 = new Product
                {
                    Name = @"Глюкометр Rightest GM 550",
                    Article = @"GM550",
                    Code1C = "ГЛ000046",
                    ProductGroup = pg2
                };

                #endregion

                #region Единицы измерения
                Measure m1 = new Measure { Name = "паков", Code1C = "12185", Rate = 1, Product = p1 };
                Measure m2 = new Measure { Name = "паков", Code1C = "12177", Rate = 1, Product = p2 };
                Measure m3 = new Measure { Name = "паков", Code1C = "2428", Rate = 1, Product = p3 };
                Measure m4 = new Measure { Name = "шт", Code1C = "01134", Rate = 1, Product = p4 };
                Measure m5 = new Measure { Name = "шт", Code1C = "13229", Rate = 1, Product = p5 };
                #endregion

                #region Цены номенклатуры
                List<ProductPrice> ProductPrices = new List<ProductPrice>
                {
                    new ProductPrice{Product = p1, PriceType = PriceType1, Measure = m1, Price = 15},
                    new ProductPrice{Product = p1, PriceType = PriceType2, Measure = m1, Price = 16},
                    new ProductPrice{Product = p2, PriceType = PriceType1, Measure = m2, Price = 20},
                    new ProductPrice{Product = p2, PriceType = PriceType2, Measure = m2, Price = 21.5M},
                    new ProductPrice{Product = p3, PriceType = PriceType1, Measure = m3, Price = 90},
                    new ProductPrice{Product = p3, PriceType = PriceType2, Measure = m3, Price = 94},
                    new ProductPrice{Product = p4, PriceType = PriceType1, Measure = m4, Price = 150},
                    new ProductPrice{Product = p4, PriceType = PriceType2, Measure = m4, Price = 160},
                    new ProductPrice{Product = p5, PriceType = PriceType1, Measure = m5, Price = 2},
                    new ProductPrice{Product = p5, PriceType = PriceType2, Measure = m5, Price = 2.2M},
                };

                #endregion

                context.PriceTypes.AddRange(PriceType1, PriceType2);
                context.Customers.AddRange(customer1, customer2);
                context.ProductGroups.AddRange(pg1, pg2);
                context.Products.AddRange(p1, p2, p3, p4, p5);
                context.Measures.AddRange(m1,m2,m3,m4,m5);
                context.ProductPrices.AddRange(ProductPrices);
                
                context.SaveChanges();


            }
        }


    }
}
