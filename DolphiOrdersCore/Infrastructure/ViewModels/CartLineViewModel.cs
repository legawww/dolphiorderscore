﻿using DolphiOrdersCore.Models.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DolphiOrdersCore.Infrastructure.ViewModels
{
    public class CartLineViewModel
    {
        public CartLine CartLine { get; set; }
        public string ReturnUrl { get; set; }
        public decimal TotalQuantity { get; set; }
        public decimal TotalSum { get; set; }
    }
}
