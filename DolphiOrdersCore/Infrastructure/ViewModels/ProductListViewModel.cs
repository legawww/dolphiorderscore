﻿using DolphiOrdersCore.Infrastructure;
using DolphiOrdersCore.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DolphiOrdersCore.Infrastructure.ViewModels
{
    public class ProductListViewModel
    {
        public IEnumerable<ProductSummaryModel> ProductSummaries { get; set; }
        public PagingInfo PagingInfo { get; set; }
        public Guid CurrentProductGroupId { get; set; }
    }
}
