﻿using DolphiOrdersCore.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DolphiOrdersCore.Infrastructure.ViewModels
{
    public class MenuProductGroupsViewModel
    {
        public IEnumerable<ProductGroup> ProductGroups { get; set; }
        public Guid CurrentProductGroupId { get; set; }
    }
}
