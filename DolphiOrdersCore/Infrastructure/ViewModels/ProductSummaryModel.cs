﻿using DolphiOrdersCore.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DolphiOrdersCore.Infrastructure.ViewModels
{
    public class ProductSummaryModel
    {
        public Product Product { get; set; }
        public decimal Price { get; set; }
    }
}
