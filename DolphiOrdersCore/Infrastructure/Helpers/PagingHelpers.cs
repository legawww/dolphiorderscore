﻿using Microsoft.AspNetCore.Html;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.Encodings.Web;
using System.Threading.Tasks;

namespace DolphiOrdersCore.Infrastructure.Helpers
{
    public static class PagingHelpers
    {
        public static HtmlString PageLinks(this IHtmlHelper html, PagingInfo pagingInfo, Func<int, string> pageUrl ) {
            TagBuilder div = new TagBuilder("div");
            div.AddCssClass("pull-center");
            for (int i = 1; i <= pagingInfo.TotalPages; i++)
            {
                //< a href = @Url.Action("List", new { page = (i + 1) }) >@(i + 1) </ a >
                TagBuilder a = new TagBuilder("a");
                a.MergeAttribute("href", pageUrl(i));
                a.InnerHtml.Append((i).ToString());
                if (pagingInfo.CurrentPage == i)
                {
                    a.AddCssClass("selected");
                    a.AddCssClass("btn-primary");
                }
                a.AddCssClass("btn btn-default");
                div.InnerHtml.AppendHtml(a);
            }
            var writer = new StringWriter();
            div.WriteTo(writer, HtmlEncoder.Default);
            return new HtmlString(writer.ToString());
        }
    }
}
