﻿using DolphiOrdersCore.Infrastructure.ViewModels;
using DolphiOrdersCore.Models.Abstract;
using DolphiOrdersCore.Models.Concrete;
using DolphiOrdersCore.Models.Entities;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DolphiOrdersCore.Components
{
    public class Navigation : ViewComponent
    {
        private readonly IProductGroupRepository _productGroupRepository;
        public Navigation(IProductGroupRepository productGroupRepository)
        {
            _productGroupRepository = productGroupRepository;
        }

        public IViewComponentResult Invoke()
        {
            Guid currentProductGroupId = Guid.Empty;
            Guid id;
            if (RouteData.Values.ContainsKey("productGroupId") && RouteData.Values["productGroupId"] != null)
            {
                if (Guid.TryParse(RouteData.Values["productGroupId"].ToString(), out id))
                {
                    currentProductGroupId = id;
                }
            }

            return View("Menu", new MenuProductGroupsViewModel { ProductGroups = _productGroupRepository.ProductGroups, CurrentProductGroupId = currentProductGroupId });
        }
    }
}
