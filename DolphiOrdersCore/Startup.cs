﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DolphiOrdersCore.Infrastructure.ModelBinders;
using DolphiOrdersCore.Miscellaneous;
using DolphiOrdersCore.Models.Abstract;
using DolphiOrdersCore.Models.Concrete;
using DolphiOrdersCore.Models.Entities;
using DolphiOrdersCore.Models.Infrastructure;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace DolphiOrdersCore
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            //Project's data dependencies
            string connection = Configuration.GetConnectionString("DefaultConnection");
            services.AddDbContext<ProductContext>(options => options.UseSqlServer(connection));
            services.AddScoped<Exchanger1C>();
            services.AddScoped<IProductGroupRepository, EFProductGroupRepository>();
            services.AddScoped<IProductRepository, EFProductRepository>();

            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.AddDistributedMemoryCache();
            services.AddSession();

            services.AddIdentity<User, IdentityRole>()
                .AddEntityFrameworkStores<ProductContext>();

            services.AddMvc(opts =>
            {
                opts.ModelBinderProviders.Insert(0, new CartmModelBinderProvider());
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            if (env.IsDevelopment())
            {
                loggerFactory.AddDebug();

                app.UseBrowserLink();
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }

            app.UseStaticFiles();
            app.UseAuthentication();
            app.UseSession();
            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "empty",
                    template: "",
                    defaults: new { controller = "Home", action = "List", productGroupId = (int?)null, page = 1 });
                routes.MapRoute(
                    name: "Pages",
                    template: "Page{page}",
                    defaults: new { controller = "Home", action = "List", productGroupId = (int?)null });
                routes.MapRoute(
                    name: "ProductGroups",
                    template: "Group{productGroupId}",
                    defaults: new { controller = "Home", action = "List", page = 1 });
                routes.MapRoute(
                    name: "ProductGroupsAndPages",
                    template: "Group{productGroupId}/Page{page}",
                    defaults: new { controller = "Home", action = "List" });
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=list}");
            });
        }
    }
}
