﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using DolphiOrdersCore.Miscellaneous;
using DolphiOrdersCore.Models.Concrete;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace DolphiOrdersCore.Controllers
{
    public class FileUploadController : Controller
    {
        IHostingEnvironment _appEnvironment;
        ProductContext db;
        Exchanger1C _exchanger;

        public FileUploadController(IHostingEnvironment appEnvironment, ProductContext context, Exchanger1C exchanger)
        {
            _appEnvironment = appEnvironment;
            db = context;
            _exchanger = exchanger;
        }

        public IActionResult UploadDialog(string title)
        {
            ViewData["Title"] = title;
            return View();
        }

        [HttpPost]
        public IActionResult UploadExchangeFile(IFormFile uploadedFile)
        {
            Stream stream =  uploadedFile.OpenReadStream();
            StreamReader reader = new StreamReader(stream);
            string filesData = reader.ReadToEnd();
            string res = _exchanger.PerformExchangeWith1C(filesData);
            return Content(res);
        }
    }
}