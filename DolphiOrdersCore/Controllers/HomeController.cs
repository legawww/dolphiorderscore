﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using DolphiOrdersCore.Models;
using DolphiOrdersCore.Models.Concrete;
using DolphiOrdersCore.Models.Entities;
using DolphiOrdersCore.Models.Infrastructure;
using DolphiOrdersCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.Extensions.Logging;
using DolphiOrdersCore.Infrastructure.ViewModels;

namespace DolphiOrdersCore.Controllers
{
    public class HomeController : Controller
    {
        ProductContext db;
        public int PageSize = 10;

        public HomeController(ProductContext context)
        {
            db = context;
            //db.GetService<ILoggerFactory>().AddProvider(new MyLoggerProvider());
        }

        public IActionResult List(Guid productGroupId, int page = 1)
        {
            ProductListViewModel productListViewModel = new ProductListViewModel
            {
                ProductSummaries = db.Products
                .OrderBy(p => p.Id)
                .SelectMany(p=>db.ProductPrices.Where(pp => pp.PriceTypeId == Guid.Empty && p.Id== pp.ProductId).DefaultIfEmpty(), (p, pp) => new ProductSummaryModel { Product = p, Price = pp.Price!=null? pp.Price:0 })
                .Where(ps => productGroupId == Guid.Empty || ps.Product.ProductGroup.Id == productGroupId)
                .Skip((page - 1) * PageSize)
                .Take(PageSize),

                PagingInfo = new PagingInfo { CurrentPage = page, ItemsPerPage = PageSize, TotalsItems = db.Products.Count(p => productGroupId == Guid.Empty || p.ProductGroup.Id == productGroupId) },

                CurrentProductGroupId = productGroupId
                
            };
            return View(productListViewModel);
        }

        [HttpGet]
        public IActionResult Buy(Guid id, Cart cart)
        {
            if (id == null) return BadRequest("Product not found");
            cart.AddItem(new Product { Id = (Guid)id }, 1, 10);
            ViewBag.ProductId = id;
            return View();
        }

        [HttpPost]
        public IActionResult Buy(Order order)
        {

            return View();
        }




        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
