﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DolphiOrdersCore.Miscellaneous;
using Microsoft.AspNetCore.Mvc;

namespace DolphiOrdersCore.Controllers
{
    public class ExchangeController : Controller
    {
        private readonly Exchanger1C _exchanger;
        public ExchangeController(Exchanger1C exchanger)
        {
            _exchanger = exchanger;
        }
        [HttpPost]
        public JsonResult DoExchange([FromForm]string inputMessage)
        {
            var outputMessage = _exchanger.PerformExchangeWith1C(inputMessage);
            return Json(outputMessage);
        }
        public IActionResult Test(string param)
        {
            return Json(param);
        }

    }
}