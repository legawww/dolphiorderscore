﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DolphiOrdersCore.Infrastructure.ViewModels;
using DolphiOrdersCore.Miscellaneous;
using DolphiOrdersCore.Models.Concrete;
using DolphiOrdersCore.Models.Entities;
using DolphiOrdersCore.Models.Infrastructure;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ViewFeatures;

namespace DolphiOrdersCore.Controllers
{
    public class CartController : Controller
    {
        ProductContext db;

        public CartController(ProductContext context)
        {
            db = context;
        }
        public IActionResult Index(Cart cart, string rU)
        {
            return View(new CartViewModel { Cart = cart, ReturnUrl = rU });
        }
        [HttpPost]
        public IActionResult AddToCart(Cart cart, int id, decimal price, string returnUrl)
        {
            Product product = db.Products.Find(id);
            if (product != null)
            {
                cart.AddItem(product, 1, price);
                SetCartIntoSession(cart);
            }
            return RedirectToAction("Index", new { rU = returnUrl });
        }

        public IActionResult RemoveFromCart(Cart cart, int id, string returnUrl)
        {
            Product product = db.Products.Find(id);
            if (product != null)
            {
                cart.RemoveItem(product);
                SetCartIntoSession(cart);
            }
            return RedirectToAction("Index", new { rU = returnUrl });
        }

        public IActionResult SetProductQuantity(Cart cart, Guid id, int quantity, string returnUrl)
        {
            var line = cart.SetProductQuantity(id, quantity);
            SetCartIntoSession(cart);

            return PartialView("CartLinePartial", new CartLineViewModel() {
                CartLine = line,
                ReturnUrl = returnUrl,
                TotalQuantity = cart.GetTotalQuantity(),
                TotalSum = cart.GetTotalSum()
            });
            
        }
        private void SetCartIntoSession(Cart cart)
        {
            if (cart != null)
            {
                HttpContext.Session.SetObjectAsJson("Cart", cart);
            }
        }
    }
}