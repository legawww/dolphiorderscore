﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace DolphiOrdersCore.Migrations
{
    public partial class AddProductGroupId : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Customers_PriceTypes_PriceTypeId",
                table: "Customers");

            migrationBuilder.AlterColumn<Guid>(
                name: "PriceTypeId",
                table: "Customers",
                nullable: true,
                oldClrType: typeof(Guid));

            migrationBuilder.AddColumn<Guid>(
                name: "TradeGroupId",
                table: "Customers",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Customers_TradeGroupId",
                table: "Customers",
                column: "TradeGroupId");

            migrationBuilder.AddForeignKey(
                name: "FK_Customers_PriceTypes_PriceTypeId",
                table: "Customers",
                column: "PriceTypeId",
                principalTable: "PriceTypes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Customers_Customers_TradeGroupId",
                table: "Customers",
                column: "TradeGroupId",
                principalTable: "Customers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Customers_PriceTypes_PriceTypeId",
                table: "Customers");

            migrationBuilder.DropForeignKey(
                name: "FK_Customers_Customers_TradeGroupId",
                table: "Customers");

            migrationBuilder.DropIndex(
                name: "IX_Customers_TradeGroupId",
                table: "Customers");

            migrationBuilder.DropColumn(
                name: "TradeGroupId",
                table: "Customers");

            migrationBuilder.AlterColumn<Guid>(
                name: "PriceTypeId",
                table: "Customers",
                nullable: false,
                oldClrType: typeof(Guid),
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Customers_PriceTypes_PriceTypeId",
                table: "Customers",
                column: "PriceTypeId",
                principalTable: "PriceTypes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
