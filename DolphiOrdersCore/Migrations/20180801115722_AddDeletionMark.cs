﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace DolphiOrdersCore.Migrations
{
    public partial class AddDeletionMark : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "DeletionMark",
                table: "Products",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "DeletionMark",
                table: "ProductGroups",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "DeletionMark",
                table: "PriceTypes",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "DeletionMark",
                table: "Outlets",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "DeletionMark",
                table: "Orders",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "DeletionMark",
                table: "Measures",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "DeletionMark",
                table: "Customers",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DeletionMark",
                table: "Products");

            migrationBuilder.DropColumn(
                name: "DeletionMark",
                table: "ProductGroups");

            migrationBuilder.DropColumn(
                name: "DeletionMark",
                table: "PriceTypes");

            migrationBuilder.DropColumn(
                name: "DeletionMark",
                table: "Outlets");

            migrationBuilder.DropColumn(
                name: "DeletionMark",
                table: "Orders");

            migrationBuilder.DropColumn(
                name: "DeletionMark",
                table: "Measures");

            migrationBuilder.DropColumn(
                name: "DeletionMark",
                table: "Customers");
        }
    }
}
