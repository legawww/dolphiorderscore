﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace DolphiOrdersCore.Migrations
{
    public partial class AddMeasureInProduct : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<Guid>(
                name: "MeasureId",
                table: "Products",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Products_MeasureId",
                table: "Products",
                column: "MeasureId",
                unique: true,
                filter: "[MeasureId] IS NOT NULL");

            migrationBuilder.AddForeignKey(
                name: "FK_Products_Measures_MeasureId",
                table: "Products",
                column: "MeasureId",
                principalTable: "Measures",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Products_Measures_MeasureId",
                table: "Products");

            migrationBuilder.DropIndex(
                name: "IX_Products_MeasureId",
                table: "Products");
            
        }
    }
}
