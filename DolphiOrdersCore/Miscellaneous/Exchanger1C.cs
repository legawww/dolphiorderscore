﻿using DolphiOrdersCore.Models.Abstract;
using DolphiOrdersCore.Models.Concrete;
using DolphiOrdersCore.Models.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DolphiOrdersCore.Miscellaneous
{
    public class Exchanger1C
    {
        private readonly ProductContext db;
        private readonly ILogger<Exchanger1C> _logger;
        public Exchanger1C(ProductContext context, ILogger<Exchanger1C> logger)
        {
            db = context;
            _logger = logger;
        }

        public string PerformExchangeWith1C(string inputMessage)
        {
            JObject json = JObject.Parse(inputMessage);
            IList<JToken> results;

            // ProductGroups
            var jPG = json["Catalog.ProductGroups"];
            if (jPG!=null)
            {
                results = jPG.Children().ToList();
                ProductGroup productGroup;
                // serialize JSON results into .NET objects
                foreach (JToken result in results)
                {
                    // JToken.ToObject is a helper method that uses JsonSerializer internally
                    ProductGroup searchResultPG = result.ToObject<ProductGroup>();
                    
                    //_logger.LogDebug("Old Id:{0}", searchResult.Id);
                    productGroup = db.ProductGroups.Find(searchResultPG.Id);
                    if (productGroup == null)
                    {
                        db.ProductGroups.Add(searchResultPG);
                    }
                    else
                    {
                        productGroup.FillForExchanger(searchResultPG);
                    }
                    //_logger.LogDebug("New Id:{0}", newId);
                    //_logger.LogInformation("Id in object after creation:{0}", searchResult.Id);
                }
            }

            // Products
            var jP = json["Catalog.Products"];
            if (jP!=null)
            {
                results = jP.Children().ToList();
                Product product;
                foreach (JToken result in results)
                {
                    // JToken.ToObject is a helper method that uses JsonSerializer internally
                    Product searchResultP = result.ToObject<Product>();

                    //if product group is not exist, then create it with name "Object has not found"
                    if (searchResultP.ProductGroupId != null)
                    {
                        if (db.ProductGroups.Find(searchResultP.ProductGroupId) == null)
                        {
                            ProductGroup pg = new ProductGroup { Id = (Guid)searchResultP.ProductGroupId, Name = "Object has not found" };
                            db.ProductGroups.Add(pg);
                        }
                        
                    }

                    //if Parent is not exist, then create it with name "Object has not found"
                    if (searchResultP.ParentId != null)
                    {
                        if (db.Products.Find(searchResultP.ParentId) == null)
                        {
                            Product p = new Product { Id = (Guid)searchResultP.ParentId, Name = "Object has not found" };
                            db.Products.Add(p);
                        }

                    }

                    product = db.Products.Find(searchResultP.Id);
                    if (product == null)
                    {
                        db.Products.Add(searchResultP);
                    }
                    else
                    {
                        product.FillForExchanger(searchResultP);
                    }
                }
            }

            // Measures
            var jM = json["Catalog.Measures"];
            if (jM != null)
            {
                results = jM.Children().ToList();
                Measure measure;
                foreach (JToken result in results)
                {
                    // JToken.ToObject is a helper method that uses JsonSerializer internally
                    Measure searchResult = result.ToObject<Measure>();

                    //if Parent is not exist, then create it with name "Object has not found"
                    if (searchResult.ProductId != null)
                    {
                        if (db.Products.Find(searchResult.ProductId) == null)
                        {
                            Product p = new Product { Id = (Guid)searchResult.ProductId, Name = "Object has not found" };
                            db.Products.Add(p);
                        }
                    }

                    measure = db.Measures.Find(searchResult.Id);
                    if (measure == null)
                    {
                        db.Measures.Add(searchResult);
                    }
                    else
                    {
                        measure.FillForExchanger(searchResult);
                    }
                }
            }

            db.SaveChanges();

            return JsonConvert.SerializeObject("OK", Formatting.Indented);
        }
    }
}
